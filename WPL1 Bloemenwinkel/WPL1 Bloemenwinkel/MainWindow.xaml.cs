﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPL1_Bloemenwinkel
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void imgRoos_MouseEnter(object sender, MouseEventArgs e)
        {
            mimosa.Background = new SolidColorBrush(Color.FromRgb(255, 0, 0));
        }

        private void imgBitter_MouseEnter(object sender, MouseEventArgs e)
        {
            
            mimosa.Background = new SolidColorBrush(Color.FromRgb(255, 255, 0));
        }

        private void imgGouden_MouseEnter(object sender, MouseEventArgs e)
        {
            
            mimosa.Background = new SolidColorBrush(Color.FromRgb(252, 173, 3));
        }

        private void imgLavendel_MouseEnter(object sender, MouseEventArgs e)
        {
            mimosa.Background = new SolidColorBrush(Color.FromRgb(194, 3, 252));
        }

        private void imgRoos_MouseLeave(object sender, MouseEventArgs e)
        {
            mimosa.Background = new SolidColorBrush(Color.FromRgb(156, 156, 156));
        }

        private void imgBitter_MouseLeave(object sender, MouseEventArgs e)
        {
            mimosa.Background = new SolidColorBrush(Color.FromRgb(156, 156, 156));
        }

        private void imgGouden_MouseLeave(object sender, MouseEventArgs e)
        {
            mimosa.Background = new SolidColorBrush(Color.FromRgb(156, 156, 156));
        }

        private void imgLavendel_MouseLeave(object sender, MouseEventArgs e)
        {
            mimosa.Background = new SolidColorBrush(Color.FromRgb(156, 156, 156));
        }
    }
}
